import 'package:flutter/material.dart';

import 'main.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _screenNumber = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: navItems[_screenNumber].screen,
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: navItems
            .map((navItem) => BottomNavigationBarItem(
                  icon: navItem.navIcon,
                  title: navItem.title,
                ))
            .toList(),
        currentIndex: _screenNumber,
        onTap: (i) => setState(() {
          _screenNumber = i;
        }),
      ),
    );
  }
}
