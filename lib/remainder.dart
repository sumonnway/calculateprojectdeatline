import 'package:flutter/material.dart';

class Remainder extends StatefulWidget {
  final String title;
  Remainder({this.title});
  @override
  _RemainderState createState() => _RemainderState();
}

class _RemainderState extends State<Remainder> {
  String totalDateFromStartedToNow = '';
  String totalDateFromNowTo50Per = '';
  String totalDateFromNowTo100Per = '';

  @override
  void initState() {
    calculateDateTotal();
    calculateDate50PerRemaining();
    calculateDate100PerRemaining();
    super.initState();
  }

  void calculateDateTotal() {
    final startDate = DateTime(2021, 12, 12);
    final date2 = DateTime.now();
    totalDateFromStartedToNow = (date2.difference(startDate).inDays).toString();
  }

  void calculateDate50PerRemaining() {
    final startDate = DateTime(2022, 2, 14);
    final date2 = DateTime.now();
    totalDateFromNowTo50Per = (startDate.difference(date2).inDays).toString();
  }

  void calculateDate100PerRemaining() {
    final startDate = DateTime(2022, 4, 14);
    final date2 = DateTime.now();
    totalDateFromNowTo100Per = (startDate.difference(date2).inDays).toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: Container(
        child: ListView(
          padding: EdgeInsets.fromLTRB(0, 48, 0, 16),
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(48, 24, 48, 0),
              child: Text(
                "You spent",
                style: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 24,
                    fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 36),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    width: 140,
                    height: 140,
                    child: CircularProgressIndicator(
                        backgroundColor: Colors.purple[100],
                        value: 0.3,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.purple),
                        strokeWidth: 9),
                  ),
                  Container(
                    width: 165,
                    height: 165,
                    child: CircularProgressIndicator(
                        backgroundColor: Colors.pink[100],
                        value: 0.5,
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.pink),
                        strokeWidth: 9),
                  ),
                  Container(
                    width: 190,
                    height: 190,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(40))),
                    child: CircularProgressIndicator(
                        backgroundColor: Colors.blue[100],
                        value: 0.7,
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                        strokeWidth: 9),
                  ),
                  Container(
                      width: 120,
                      height: 60,
                      child: Center(
                        child: Text(
                          '$totalDateFromStartedToNow \nDays',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      ))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(32, 48, 32, 0),
              child: Column(
                children: [
                  singleElement(
                      color: Colors.purple,
                      type: "Total",
                      inGram: "123",
                      inPercentage: "v"),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: singleElement(
                        color: Colors.pink,
                        type: "First Time",
                        inGram: "64",
                        inPercentage: "v1"),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: singleElement(
                        color: Colors.blue,
                        type: "Final",
                        inGram: "59",
                        inPercentage: "v2"),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: singleElement(
                        color: Colors.orange[500],
                        type: "v0=>v1",
                        inGram: "$totalDateFromNowTo50Per",
                        inPercentage: "v1"),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: singleElement(
                        color: Colors.green[500],
                        type: "v0=>v2",
                        inGram: "$totalDateFromNowTo100Per",
                        inPercentage: "v2"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget singleElement(
      {Color color, String type, String inGram, String inPercentage}) {
    return Container(
      child: Row(
        children: [
          Container(
            width: 12,
            height: 12,
            decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.all(Radius.circular(4))),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 16),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      type,
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        inGram + " Days",
                      ),
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        inPercentage,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
