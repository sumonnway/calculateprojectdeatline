import 'package:flutter/material.dart';
import 'package:remainder/calendar.dart';
import 'package:remainder/remainder.dart';

import 'home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Remainder',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class NavObject {
  Widget screen;
  Icon navIcon;
  Text title;
  NavObject({this.screen, this.navIcon, this.title});
}

List<NavObject> navItems = [
  NavObject(
    screen: Remainder(title: "Remainder"),
    navIcon: Icon(Icons.alarm),
    title: Text('Remainder'),
  ),
  NavObject(
    screen: Calendar(),
    navIcon: Icon(Icons.date_range),
    title: Text('Calendar'),
  ),
  // NavObject(
  //   screen: Remainder(title: "Share"),
  //   navIcon: Icon(Icons.share),
  //   title: Text('Share'),
  // ),
];
